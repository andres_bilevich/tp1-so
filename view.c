#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/stat.h>
#include <sys/shm.h> 
#include <semaphore.h> 
#include <sys/mman.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <fcntl.h> 

#define MSGSIZE 512
#define NAME "shmem"
#define SEM_NAME "semaphore"

void error_print(char * message);
sem_t* createSemaphore(char * name);
void closeSemaphore(sem_t* sem, char * name);

int main(int argc, char * argv[]) {

   setvbuf(stdout, NULL, _IONBF, 0);

   int quantity_files, child_id, num_vars, num_clauses, ret_val;
   float cpu_time;
   char state[15];
   char file_name[40];

   if(argc > 1){
      quantity_files = atoi(argv[1]);
   }else{
      ret_val = scanf("%d", &quantity_files);
      if(ret_val == EOF) error_print("view_scanf");
   }

   int sm_size = quantity_files * MSGSIZE;

   //-------------------------link shared mem----------------------------------------------------------
   int shm_fd = shm_open(NAME, O_RDONLY, 0666);
   if (shm_fd < 0) error_print("view_shm_open");

   char * shm_address = (char*)mmap(0, sm_size, PROT_READ, MAP_SHARED, shm_fd, 0);
   if (shm_address == MAP_FAILED) error_print("view_mmap");

   //-------------------------Link semaphore-----------------------------------------------------------
   sem_t* sem = createSemaphore(SEM_NAME);
   //-------------------------wait input---------------------------------------------------------------
   int cont = 0;
   char buff[200];
   int size;

   for (size_t i = 0; i < quantity_files; i++){
      sem_wait(sem);
      size = 0;
      while(shm_address[size + cont] != '$'){
          size++;
      }
      size++;
      memcpy(buff, shm_address+cont, size);
      buff[size] = '\0';

      sscanf(buff, "@ %d %s %d %d %f %s $", &child_id, file_name, &num_vars, &num_clauses, &cpu_time, state);
      if(ret_val == EOF) error_print("view_sscanf");

      cont += size;
      printf("Child ID:%d Completed %s\t[%d\t%d\t%f\t%s]\n", child_id, file_name, num_vars, num_clauses, cpu_time, state);
   }
   printf("All %d files completed\n", quantity_files);
  
   //-------------------------Close file descriptors, shared meme and semaphores----------------------
   munmap(shm_address, sm_size);
   close(shm_fd);

   closeSemaphore(sem,SEM_NAME);

   return 0;
}

void error_print(char * message){
   perror(message);
   exit(EXIT_FAILURE);
}

sem_t* createSemaphore(char * name){
   sem_t* sem = sem_open(name, O_CREAT, 0666,0);
   if(sem == SEM_FAILED) error_print("solve_sem_open");
   return sem;
}

void closeSemaphore(sem_t* sem, char * name){
   sem_close(sem);
   sem_unlink(name);
}
