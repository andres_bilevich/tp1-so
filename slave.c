#define _BSD_SOURCE     //para el popen

#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/stat.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <fcntl.h> 

#define MSGSIZE 512

void error_print(char * message);

int main(int argc, char * argv[]) {
   int ret_val; 
   int my_id = -1;

   char buf[MSGSIZE] = "";
   char file_name[100] = "";
   char aux_buf[MSGSIZE] = "";

   int num_vars = -1;
   int num_clauses = -1;
   float cpu_time = -1;
   char state[15] = "";
   char send[200] = "";

   while (1){
      ret_val = read(0, buf, MSGSIZE); 
      switch (ret_val) {
      case -1:                                        
         if (errno == EAGAIN) {                       //pipe is empty
            break;                                    
         } else {                                     //error reading pipe
            error_print("slave_read");
         } 
      case 0:                                         //reached EOF
         exit(0); 
      default:                                        //message recived in pipe succesfully
         ret_val = sscanf(buf, "@ %d # %s $", &my_id, file_name);
         if(ret_val == EOF) error_print("slave_sscanf");

         ret_val = sprintf(send,"minisat %s | grep -o -e \"Number of.*[0-9]\\+\" -e \"CPU time.*\" -e \".*SATISFIABLE\" | tr \"\\n\" \"\t\" | tr -d \" \t\"",file_name);
         if(ret_val < 0) error_print("slave_sprintf");

         FILE * msg = popen(send,"r");
         if(msg == NULL) error_print("slave_popen");

         if(fgets(aux_buf,MSGSIZE,msg) == NULL) error_print("slave_fgets");
         pclose(msg);

         ret_val = sscanf(aux_buf, "Numberofvariables:%dNumberofclauses:%dCPUtime:%fs%s", &num_vars, &num_clauses, &cpu_time, state);
         if(ret_val == EOF) error_print("slave_sscanf");

         ret_val = sprintf(buf, "@ %d %s %d %d %f %s $", my_id, file_name, num_vars, num_clauses, cpu_time, state);
         if(ret_val < 0) error_print("slave_sprintf");

         write(1, buf, MSGSIZE);
      } 
   }

   return 0;

}

void error_print(char * message){
   perror(message);
   exit(EXIT_FAILURE);
}