CC=gcc
CFLAGS= -pedantic -std=c99 -Wall  -D_DEFAULT_SOURCE -D_XOPEN_SOURCE=500 -lrt -pthread

all: solve slave view

solve: solve.c
	@$(CC) $(CFLAGS) solve.c -o solve

slave: slave.c
	@$(CC) $(CFLAGS) slave.c -o slave

view: view.c
	@$(CC) $(CFLAGS) view.c -o view

clean:
	@rm solve slave view

.PHONY: all clean
