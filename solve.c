#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/stat.h>
#include <sys/shm.h> 
#include <semaphore.h> 
#include <sys/mman.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <fcntl.h> 

#define MAX_SLAVES 5
#define MSGSIZE 512
#define NAME "shmem"
#define SEM_NAME "semaphore"

typedef struct child{
      int pid;
      int fd;
      int ongoing_tasks;
} child;

child childs[MAX_SLAVES];
int file_index = 0;

int addTask(int index, char * file_name);
void error_print(char * message);
sem_t* createSemaphore(char * name);
void closeSemaphore(sem_t* sem, char * name);

int main(int argc, char * argv[]) {

   //para que prinetee en el fd sin pasar por un bufer
   setvbuf(stdout, NULL, _IONBF, 0);

   int quantity_files = argc-1;
   int tasks_completed = 0;
   int max_ongoing_tasks = 4;
   int ret_val;
   
   int sm_size = quantity_files * MSGSIZE;

   //in case solve closed unexpectedly and didn't unlink
   shm_unlink(NAME);
   sem_unlink(SEM_NAME);
   
   //-------------------------Create shared mem--------------------------------------------------------
   int shm_fd = shm_open(NAME, O_CREAT | O_EXCL | O_RDWR , 0666);
   if (shm_fd < 0) error_print("solve_shm_open");

   if (ftruncate(shm_fd, sm_size) < 0) error_print("solve_shm_open");

   char * shm_address = (char*)mmap(0, sm_size, PROT_READ | PROT_WRITE, MAP_SHARED, shm_fd, 0);
   if (shm_address == MAP_FAILED) error_print("solve_mmap");

   //-------------------------Create semaphore--------------------------------------------------------
   sem_t* sem = createSemaphore(SEM_NAME);
   
   //-------------------------Send info to view and sleep----------------------------------------------
   printf("%d\n", quantity_files);
   sleep(2);

   //-------------------------Create children---------------------------------------------------------
   int fd_main[2];
   int fd_slave[2];
   char * arg_list[] = {NULL};

   if (pipe(fd_main) < 0) error_print("solve_pipe_main");
   
   for (size_t i = 0; i < MAX_SLAVES; i++){    
      
      pid_t pid;
      if (pipe(fd_slave) < 0) error_print("solve_pipe_slave");

      //hace que los reads en ese fd sean no blequeantes.
      fcntl(fd_slave[0], F_SETFL, O_NONBLOCK);

      if ( (pid = fork()) < 0) error_print("solve_fork");

      else if (pid > 0) {     //Parent
         childs[i].pid = pid;
         childs[i].fd = fd_slave[1];
         childs[i].ongoing_tasks = 0;
         close(fd_slave[0]);
      }
      else {                  //Child
         dup2(fd_slave[0], 0);
         dup2(fd_main[1], 1);
         close(fd_slave[0]);
         close(fd_slave[1]);
         close(fd_main[0]);
         close(fd_main[1]);
         execv("./slave", arg_list);
      }
      
   }
   
   //---------------------send initial files to children for them to process------------------------------------ 
   int files_asigned = 0;  
   for (size_t i = 0; i < MAX_SLAVES; i++){
      while(childs[i].ongoing_tasks < max_ongoing_tasks && files_asigned < quantity_files){
         addTask(i, argv[file_index+1]);
         files_asigned++;
      }
   }
   
   //--------------------------------waiting for responses-----------------------------------------------------
   int keep_going = 1;
   int cont = 0;
   int child_id, num_vars, num_clauses, size;
   float cpu_time;
   char state[15];
   char file_name[40];
   char aux_buff[200];
   char ans[MSGSIZE];

   FILE * fp = fopen ("./results","w"); 

   while(keep_going){
      
      ret_val = read(fd_main[0],ans,MSGSIZE);               //wait for any child to write.
      if(ret_val < 0) error_print("solve_read");

      ret_val = sscanf(ans, "@ %d %s %d %d %f %s $", &child_id, file_name, &num_vars, &num_clauses, &cpu_time, state);
      if(ret_val == EOF) error_print("solve_sscanf");

      ret_val = fprintf (fp,"Child ID:%d Completed %s\t[%d\t%d\t%f\t%s]\n", child_id, file_name, num_vars, num_clauses, cpu_time, state);
      if(ret_val < 0) error_print("solve_fprintf");
      
      ret_val = sprintf(aux_buff,"@ %d %s %d %d %f %s $", child_id, file_name, num_vars, num_clauses, cpu_time, state);
      if(ret_val < 0) error_print("solve_sprintf");

      //write in shared mem
      size = 0;
      while(aux_buff[size] != '$'){
         shm_address[size + cont] = aux_buff[size];
         size++;
      }
      shm_address[size + cont] = '$';
      cont += size + 1;

      sem_post(sem);                                                      //comunicate to view its safe to read.
     
      childs[child_id].ongoing_tasks--;
      tasks_completed++;

      if(file_index < quantity_files){                                    //check if there are still tasks to be asigned.
         addTask(child_id, argv[file_index+1]);                           //asign to same child.
      }else{   
         if(tasks_completed == quantity_files){                           //check if all tasks are done.
            keep_going = 0;
         }
      }       
   }

   //----------------------close all file descriptors, shared mem and semaphores--------------------------------------
   for (size_t i = 0; i < MAX_SLAVES; i++){                                
      close(childs[i].fd);
   }

   close(fd_main[0]);
   close(fd_main[1]);
   fclose (fp);

   munmap(shm_address, sm_size);
   close(shm_fd);
   shm_unlink(NAME);

   closeSemaphore(sem,SEM_NAME);

   //--------------------------------thats all folks----------------------------------------------------------------
   return 0;
}

int addTask(int index, char * file_name){
   childs[index].ongoing_tasks++;
   char buff[MSGSIZE] = "";
   sprintf(buff, "@ %d # %s $",index,file_name);  
   write(childs[index].fd, buff, MSGSIZE);
   file_index++;
   return 0;
}

void error_print(char * message){
   perror(message);
   exit(EXIT_FAILURE);
}

sem_t* createSemaphore(char * name){
   sem_t* sem = sem_open(name, O_CREAT, 0666,0);
   if(sem == SEM_FAILED) error_print("solve_sem_open");
   return sem;
}

void closeSemaphore(sem_t* sem, char * name){
   sem_close(sem);
   sem_unlink(name);
}











